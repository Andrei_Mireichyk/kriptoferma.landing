import "./instagram.scss";
import axios from 'axios';

let data = {
    params: {
        access_token: "7906526890.90eb249.5103bb8a423a4d08a548911c144f6cf9",
        count: 10
    }

};

axios.get('https://api.instagram.com/v1/users/self/media/recent', data)
    .then(function (response) {
        response.data.data.forEach(function (val, key) {
            pushImages(val.images.standard_resolution.url);
        });
    })
    .catch(function (e) {
        console.log(e);
    });

function pushImages(url) {
    let div = document.createElement('div');
    let img = document.createElement('img');
    let container = document.querySelector('#instagram');
    img.src = url;
    div.appendChild(img);
    container.appendChild(img );
}

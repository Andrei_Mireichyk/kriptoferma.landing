import './base.scss';


//Polyfill

//- foreach
import 'nodelist-foreach-polyfill';

//- closest
import elementClosest from 'element-closest';
elementClosest(window); // this is used to reference window.Element

// Base components
import instagram from '../instagram/instagram';

